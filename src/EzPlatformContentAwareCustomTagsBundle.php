<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformContentAwareCustomTags;

use ContextualCode\EzPlatformContentAwareCustomTags\DependencyInjection\EzPlatformContentAwareCustomTagsExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EzPlatformContentAwareCustomTagsBundle extends Bundle
{
    public function getContainerExtension(): EzPlatformContentAwareCustomTagsExtension
    {
        if (null === $this->extension) {
            $this->extension = new EzPlatformContentAwareCustomTagsExtension();
        }

        return $this->extension;
    }
}
