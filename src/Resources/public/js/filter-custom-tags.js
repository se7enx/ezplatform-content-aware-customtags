(function(eZ, AlloyEditor) {
    const disabledCustomTags = eZ.richText.contentAwareCustomTags && eZ.richText.contentAwareCustomTags.disabled;
    if (!disabledCustomTags || !disabledCustomTags.length) {
        return;
    }

    disabledCustomTags.forEach(customTag => {
            delete AlloyEditor.Buttons[customTag];
            delete AlloyEditor.Buttons[customTag + 'update'];
            delete AlloyEditor.Buttons[customTag + 'edit'];
    });
})(window.eZ, window.AlloyEditor);
