<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformContentAwareCustomTags\DependencyInjection;

use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\SiteAccessAware\Configuration as SiteAccessConfiguration;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration extends SiteAccessConfiguration
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(EzPlatformContentAwareCustomTagsExtension::ALIAS);
        $root = $treeBuilder->getRootNode()->children();

        $this
            ->addContentAwareCustomTags($root)
            ->end();

        return $treeBuilder;
    }

    /**
     * Define Content-Aware RichText Custom Tags.
     *
     * The configuration is available at:
     * <code>
     * content_aware_custom_tags:
     *     settings:
     *         - {parentLocationId: 1202, disabled: [customTag1, customTag2]}
     *         - {parentLocationId: 1411, enabled: [customTag3, customTag4, customTag5]}
     * </code>
     */
    protected function addContentAwareCustomTags(NodeBuilder $root): NodeBuilder
    {
        return $root
            ->arrayNode('settings')
                ->arrayPrototype()
                    ->children()
                        ->integerNode('parentLocationId')
                        ->end()
                        ->arrayNode('disabled')
                            ->example(['customTag1', 'customTag2'])
                            ->prototype('scalar')->end()
                        ->end()
                        ->arrayNode('enabled')
                            ->example(['customTag3', 'customTag4', 'customTag5'])
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}
