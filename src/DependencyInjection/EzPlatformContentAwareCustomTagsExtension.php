<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformContentAwareCustomTags\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class EzPlatformContentAwareCustomTagsExtension extends ConfigurableExtension
{
    public const ALIAS = 'content_aware_custom_tags';

    public function getAlias(): string
    {
        return self::ALIAS;
    }

    public function getConfiguration(array $configs, ContainerBuilder $container): Configuration
    {
        return new Configuration();
    }

    public function loadInternal(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $container->setParameter('content_aware_custom_tags_settings', $configs['settings'] ?? []);
    }
}
